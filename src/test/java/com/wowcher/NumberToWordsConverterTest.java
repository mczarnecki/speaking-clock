package com.wowcher;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumberToWordsConverterTest {
    @Test
    public void tensTest() throws Exception {
        assertEquals("ten", NumberToWordsConverter.convert(10));
        assertEquals("twenty", NumberToWordsConverter.convert(20));
        assertEquals("fifty", NumberToWordsConverter.convert(50));
    }

    @Test
    public void underTwentyTest() throws Exception {
        assertEquals("zero", NumberToWordsConverter.convert(0));
        assertEquals("four", NumberToWordsConverter.convert(4));
        assertEquals("nineteen", NumberToWordsConverter.convert(19));
    }

    @Test
    public void moreThanTwentyTest() throws Exception {
        assertEquals("fifty four", NumberToWordsConverter.convert(54));
        assertEquals("twenty one", NumberToWordsConverter.convert(21));
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeArgumentsTest() {
        NumberToWordsConverter.convert(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void toLargeNumberTest() {
        NumberToWordsConverter.convert(60);
    }

}