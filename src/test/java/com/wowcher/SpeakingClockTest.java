package com.wowcher;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class SpeakingClockTest {
    private SpeakingClock speakingClock;

    @Before
    public void  init() {
        speakingClock = new SpeakingClock();
    }

    @Test
    public void speakTest() throws Exception {
        assertEquals("It's eight thirty four", speakingClock.speak("08:34"));
        assertEquals("It's ten o'clock", speakingClock.speak("10:00"));
        assertEquals("It's Midnight", speakingClock.speak("00:00"));
        assertEquals("It's Midday", speakingClock.speak("12:00"));
    }

    @Test
    public void shouldNotThrowException() {
        for (int h = 0; h < 24; h++) {
            for (int m = 0; m < 60; m++) {
                speakingClock.speak(String.format("%02d:%02d", h, m));
            }
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidTimeFormatTest() {
        speakingClock.speak("junk");
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullArgumentTest() {
        speakingClock.speak(null);
    }

}