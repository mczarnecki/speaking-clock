package com.wowcher;

import com.google.common.base.Preconditions;

public class NumberToWordsConverter {
    private static final String[] tens = {
            "zero",
            "ten",
            "twenty",
            "thirty",
            "forty",
            "fifty",
    };
    private static final String[] numbers = {
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen"
    };

    /**
     * Converts integer from [0-59] range into words.
     * @param number input number
     * @return {@code number} in words
     * @throws IllegalArgumentException if {@code number} number is out of range
     */
    public static String convert(final int number) {
        Preconditions.checkArgument(number >= 0 && number <= 59, "Converter accepts numbers form [0-59] range only");

        String result;
        if (number % 10 == 0) {
            result = tens[number / 10];
        } else if (number < 20) {
            result = numbers[number];
        } else {
            result = tens[number / 10] + " " + numbers[number - number / 10 * 10];
        }

        return result;
    }
}
