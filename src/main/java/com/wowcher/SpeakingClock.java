package com.wowcher;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.time.DateTimeException;
import java.time.LocalTime;

public class SpeakingClock {
    private final static String WRONG_FORMAT = "Incorrect time. Expected format: MM:HH";

    /**
     * Converts 24-clock time to words
     * @param stringTime input time in HH:MM format
     * @throws IllegalArgumentException if {@code stringTime} time is malformed, null or empty
     */
    public String speak(final String stringTime) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(stringTime), WRONG_FORMAT);
        LocalTime time;
        try {
            time = LocalTime.parse(stringTime);
        } catch (DateTimeException e) {
          throw new IllegalArgumentException(WRONG_FORMAT);
        }

        StringBuilder result = new StringBuilder();
        result.append("It's ");

        if (time.equals(LocalTime.NOON)) {
            result.append("Midday");
        } else if (time.equals(LocalTime.MIDNIGHT)) {
            result.append("Midnight");
        } else if (time.getMinute() == 0) {
            result.append(NumberToWordsConverter.convert(time.getHour()))
                    .append(" o'clock");
        } else {
            result.append(NumberToWordsConverter.convert(time.getHour()))
                    .append(" ")
                    .append(NumberToWordsConverter.convert(time.getMinute()));
        }

        return result.toString();
    }

}
